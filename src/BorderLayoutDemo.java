import javax.swing.*;
import java.awt.*;
public class BorderLayoutDemo extends JFrame
{
	public BorderLayoutDemo() 
	{
		setLayout(new BorderLayout());		
		setSize(new Dimension(290,110));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		setLocationRelativeTo(null);
		setTitle("BorderLayout");
		
		JButton button1=new JButton("Button 1");
		JButton button2=new JButton("Button 2");
		JButton button3=new JButton("Button 3");
		JButton button4=new JButton("Button 4");
		JButton button5=new JButton("Button 5");
		JButton button6=new JButton("Button 6");
		JPanel panel1= new JPanel();
		JPanel panel2= new JPanel();
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel2.add(button4);
		panel2.add(button5);
		panel2.add(button6);
		//panel1.setLayout(new FlowLayout());	
		//panel2.setLayout(new FlowLayout());	
		add(panel2,BorderLayout.CENTER);
		add(panel1,BorderLayout.SOUTH);
		
		
	}
	public static void main(String[] args) 
	{
		BorderLayoutDemo borderLayoutTest = new BorderLayoutDemo();
	}
}
